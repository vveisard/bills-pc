**vveisard/bills-pc**</br>
_Technology is incredible_!

# Summary

Web app emulating Bill's PC using [PokéAPI](https://pokeapi.co/).

# Goals

Portfolio piece.

- showcase [Solid.js](https://www.solidjs.com/)
- showcase [Tailwind CSS](https://tailwindcss.com/)
- showcase [vite](https://vitejs.dev/)
- learn [GraphQL](https://graphql.org/)
- learn [progressive web apps](https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps)

# Development

## Environment

Install [bun](https://bun.sh/)

## Setup

Install packages:

```bash
bun install
```

## Start

```bash
bun run dev
```

OR

```bash
bun run start
```

## Format

```bash
bun run format
```

## Lint

```bash
bun run lint
```
